# 文件上传

## 项目介绍

文件上传组件

## 项目展示

### 1. FileUploader.vue

![Image 文件上传](./img/1.png)

### 2. PictureUpload.vue

![Image 图片上传](./img/2.png)


## 项目操作

### 初始化
```
npm install
```

### 启动服务
```
npm run serve
```

### 打包
```
npm run build
```

## 项目结构

- public
    - css   （放置公共css文件）
    - font  （放置字体文件）
- src
    - assets 
        - css   （放置scss公共文件）
        - js    （放置常用的js）
            - axios.js  （全局请求拦截）
            - filter.js （全局过滤器）
            - util.js   （常用的封装函数）
- vue.config.js （webpack配置）
- samples       （后台服务）

## 项目日志

### 2021.06.22

#### 1. FileUploader.vue

基于vue-simple-uploader插件的文件上传功能。  

其主要特点就是：

- 支持文件、多文件、文件夹上传

- 支持拖拽文件、文件夹上传

- 统一对待文件和文件夹，方便操作管理

- 可暂停、继续上传

- 错误处理

- 支持“快传”，通过文件判断服务端是否已存在从而实现“快传”

- 上传队列管理，支持最大并发上传

- 分块上传

- 支持进度、预估剩余时间、出错自动重试、重传等操作

##### 安装

通过npm安装：npm install vue-simple-uploader --save即可。

##### 使用

###### main.js  
```
import uploader from 'vue-simple-uploader'  

Vue.use(uploader)  
```



#### 2. PictureUpload.vue

基于input[type:file]的文件上传功能。  

主要用于可控制的图片上传，是把本地图片转成base64数据，在进行处理。 

## 总结

如果只是单纯用于上传图片可以使用这个PictureUpload。  

如果是视频分块上传或者多文件上传，使用FileUploader。

## 参考文档
[vue-simple-uploader](https://github.com/simple-uploader/Uploader)  
[强大的 Vue 上传组件 vue-simple-uploader](https://segmentfault.com/a/1190000010826214)  
[HTML input accept 属性 (文件上传类型控制)](https://www.w3cschool.cn/htmltags/att-input-accept.html)  

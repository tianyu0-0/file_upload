const webpack = require('webpack');
// 配置链接 https://cli.vuejs.org/zh/config/ 
module.exports = {
  // 部署应用包时的基本 URL
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',
  // 是否需要生产环境的 source map
  productionSourceMap: false,
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        // 设置网页标题
        args[0].title = '文件上传'
        return args
      })
    // 全局引用axios
    config.plugin('provide').use(webpack.ProvidePlugin, [{
      axios: 'axios',
    }]);
  },
  css: {
    loaderOptions: {
      // 引用并配置postcss-px2rem
      postcss: {
        plugins: [
          require('postcss-px2rem')({ // 配置项，详见官方文档
            remUnit: 100
          }), // 换算的基数
        ]
      },
      sass: {
        // 引用sass全局文件
        // 根据自己样式文件的位置调整
        prependData: `@import "@/assets/css/scss.scss";`
      }
    }
  },
}
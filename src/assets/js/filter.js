import Moment from 'moment'
// 全局过滤器
const vueFilter = {
    // 时间过滤器 自定义格式
    dateFilter: (value = new Date(), str) => {
        Moment.locale('zh-CN');
        return Moment(value).format(str)
    }
}
export default vueFilter
import router from '@/router'
import $Vue from '@/main.js'
// 请求拦截器
axios.interceptors.request.use(function (config) {
    // 获取token
    let token = sessionStorage.getItem('token') || null;
    // 将token添加到请求头headers的Authorization里
    config.headers.common['Authorization'] = token;
    // 显示加载动画
    $Vue.$Spin.show();
    return config;
}, function (error) {
    // Do something with request error
    console.error("请求拦截器error: ", error);
    return Promise.reject(error);
});
// 响应拦截器
axios.interceptors.response.use(res => {
    // 隐藏加载动画
    $Vue.$Spin.hide();
    // 返回值判断
    if (res.data.statusCode === 401) {
        // sessionStorage.setItem('token', '');
        // router.replace('/login');
        return res;
    } else {
        return res;
    }
}, function (err) {
    // 隐藏加载动画
    $Vue.$Spin.hide();
    // 错误处理
    console.error("响应拦截器error: ", error);
});
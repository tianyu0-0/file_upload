import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {
  debounce
} from './assets/js/util'
import "./assets/js/axios";
import vueFilter from './assets/js/filter'
import uploader from 'vue-simple-uploader'

Vue.use(uploader)

import {
  Button,
  Input,
  Message,
  Spin,
} from 'view-design';
import 'view-design/dist/styles/iview.css';

// 引用iview组件
Vue.component('Button', Button);
Vue.component('Input', Input);
Vue.prototype.$Message = Message;
Vue.prototype.$Spin = Spin;

// axios默认请求路径
axios.defaults.baseURL = "http://192.168.1.11:2002";
// 设置请求头 
axios.defaults.headers.common['Authorization'] = sessionStorage.getItem('token') || null;
// productionTip设置为 false ，可以阻止 vue 在启动时生成生产提示
Vue.config.productionTip = false;

// 全局过滤器
for (let key in vueFilter) {
  Vue.filter(key, vueFilter[key])
}

// 全局防抖处理
const on = Vue.prototype.$on;
Vue.prototype.$on = function (event, func) {
  let newFunc = func;
  if (event === 'click') {
    newFunc = debounce(func, 1000, true);
  }
  on.call(this, event, newFunc)
}

let $Vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default $Vue;
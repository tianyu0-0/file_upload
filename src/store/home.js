export let home = {
    namespaced: true,
    state: {
        postActions: null,
    },
    actions: {
        // post请求
        PostActions(store, data = {}) {
            let url = "/PostActions";
            return new Promise((resolve, reject) => {
                axios.get(url, data).then(({
                    data
                }) => {
                    store.commit('setPostActions', data);
                    resolve(data);
                }).catch((error) => {
                    if (error.response) {
                        // 请求已发出，但服务器响应的状态码不在 2xx 范围内
                        reject(error.response.data)
                    }
                });
            });
        },
        // get请求
        getActions(store, data = {}) {
            let url = "/cameras-algorithm";
            return new Promise((resolve, reject) => {
                axios.get(url, {
                    params: data
                }).then(({
                    data
                }) => {
                    resolve(data);
                }).catch((error) => {
                    if (error.response) {
                        // 请求已发出，但服务器响应的状态码不在 2xx 范围内
                        reject(error.response.data)
                    }
                });
            });
        },
    },
    mutations: {
        setPostActions(state, data = {}) {
            state.postActions = data;
        },
    }
}